# Contributing to asmonitor

We try to consider all merge requests fairly and with attention deserving to those willing to put in time and effort, but if you do not follow these rules, your contribution will be closed. We strive to ensure that the code joining the main branch is written to a high standard.

### Code Contribution Do's & Don'ts

Keeping the following in mind gives your contribution the best chance of landing!

#### Merge Requests

- **Do** start by [forking] the project.
- **Do** create a [feature branch] to work on instead of working directly on `main`. This helps to:
  - Protect the process.
  - Ensures users are aware of commits on the branch being considered for merge.
  - Allows for a location for more commits to be offered without mingling with other contributor changes.
  - Allows contributors to make progress while a MR is still being reviewed.
- **Do** follow the [50/72 rule] for Git commit messages.
- **Do** target your merge request to the **main branch**.
- **Do** specify a descriptive title to make searching for your merge request easier.
- **Do** list [verification steps] so your code is testable.
- **Do** reference associated issues in your merge request description.
- **Don't** leave your merge request description blank.
- **Don't** abandon your merge request. Being responsive helps us land your code faster.
- **Don't** submit unfinished code.

### Bug Fixes

- **Do** include reproduction steps in the form of [verification steps].
- **Do** link to any corresponding issues in your commit description.

## Bug Reports

When reporting issues:

- **Do** write a detailed description of your bug and use a descriptive title.
- **Do** include reproduction steps, stack traces, and anything that might help us fix your bug.
- **Don't** file duplicate reports. Search open issues for similar bugs before filing a new report.
- **Don't** attempt to report issues on a closed PR. New issues should be openned against the `main` branch.

Please report vulnerabilities in asmonitor directly to <security@davisconsulting.services>.

[feature branch]:https://docs.gitlab.com/ee/gitlab-basics/feature_branch_workflow.html
[forking]:https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html
[fork]:https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html
[verification steps]:https://docs.gitlab.com/ee/user/markdown.html#task-lists
