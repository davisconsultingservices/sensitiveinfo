# Use an intermediate image for installing antiword
FROM debian:buster-slim AS builder

RUN apt-get update \
    && apt-get install -y antiword \
    && rm -rf /var/lib/apt/lists/*

# Use an official Python runtime as a parent image
FROM python:3.10

# Copy antiword from the builder stage
COPY --from=builder /usr/bin/antiword /usr/bin/antiword

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY ./scripts /app

# Install needed packages
RUN pip install --upgrade pip
RUN pip install python-docx openpyxl xlrd pymupdf

# Run the Python script with the directory to scan as an argument
CMD ["python", "/app/scanner.py"]
