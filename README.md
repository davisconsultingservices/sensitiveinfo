# sensitiveinfo - Sensitive Information Scanning

This Sensitive Information Scanning container is designed to detect Personally Identifiable Information (PII), Protected Health Information (PHI), and Payment Card Information (PCI) within your digital assets, mounted on the local filesystem.

PII checks:
 * ssn
 * tax id
 * email
 * phone
 * address

PHI checks:
 * date of birth

PCI checks:
 * visa, mastercard, amex, discover

 Outputs a CSV to stdout

Please report vulnerabilities in sensitiveinfo directly to <security@davisconsulting.services>.

## Dependencies
* [Docker](https://www.docker.com/) available on the terminal prompt

## Installation
```
git clone git@gitlab.com:davisconsultingservices/sensitiveinfo.git
```

## Building the Docker container
```
docker build -t si-container .
```

## Testing the Docker container
```
docker run -v ./test:/app/mnt -it si-container
```

## Running the Docker container
```
docker run -v "/path/to/local/directory:/app/mnt" -it si-container
```

## TODOs
1) improved pattern matching for all categories
2) image analysis