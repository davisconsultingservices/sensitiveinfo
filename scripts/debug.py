import sys

DEBUG_MODE = False  # Set to True to enable debug mode

def print_debug(message):
    if DEBUG_MODE:
        print(message, file=sys.stdout)

def print_debug_error(message):
    if DEBUG_MODE:
        print(message, file=sys.stderr)

