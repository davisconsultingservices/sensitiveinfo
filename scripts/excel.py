from debug import print_debug_error
from openpyxl import load_workbook
import xlrd

def read_excel(file_path):
    try:
        if file_path.lower().endswith('.xlsx'):
            # Attempt to use openpyxl for .xlsx files
            wb = load_workbook(file_path, read_only=True, data_only=True)
            content = ""
            for sheet_name in wb.sheetnames:
                sheet = wb[sheet_name]
                for row in sheet.iter_rows(values_only=True):
                    content += " ".join(str(cell) for cell in row) + "\n"
            return content
        elif file_path.lower().endswith('.xls'):
            # Try using xlrd for .xls files
            wb_xls = xlrd.open_workbook(file_path)
            content = ""
            for sheet in wb_xls.sheets():
                for row_num in range(sheet.nrows):
                    content += " ".join(str(cell) for cell in sheet.row_values(row_num)) + "\n"
            return content
        else:
            print_debug_error(f"Unsupported Excel file format: {file_path}")
            return ""
    except Exception as e:
        print_debug_error(f"Error reading {file_path}: {e}")
        return ""
