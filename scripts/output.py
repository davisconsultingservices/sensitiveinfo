
def print_csv_results(results):
    print("Type,Path,Result")
    for key in results:
        for result in results[key]:
            output = (''.join(result[2])).strip()
            if output:
                print(f"{result[0]},{result[1]},{output}")