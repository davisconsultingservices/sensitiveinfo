import re
from word import read_doc,read_docx
from excel import read_excel
from pdf import read_pdf
from text import read_text
from zip import read_zip

def scan_for_sensitive_info(file_path):
    # Regular expressions for identifying PII, PHI, and PCI data

    # PII patterns
    ssn_pattern = re.compile(r'\b\d{3}-\d{2}-\d{4}\b')
    tax_id_pattern = re.compile(r'\b\d{9}\b')
    email_pattern = re.compile(r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b')
    phone_pattern = re.compile(r'\b(\d{10}|\(\d{3}\)\s?\d{3}[-.\s]?\d{4}|\+\d{1,2}\s?\(\d{1,4}\)\s?\d{1,}-?\d{1,})\b')
    address_pattern = re.compile(r'\b\d{1,5}\s[A-Za-z0-9\s]{1,},\s[A-Za-z\s]{1,},\s[A-Za-z\s]{1,}\s\d{5}(-\d{4})?\b')

    # Combine all PII patterns
    pii_pattern = re.compile(f"{ssn_pattern.pattern}|{tax_id_pattern.pattern}|{email_pattern.pattern}|{phone_pattern.pattern}|{address_pattern.pattern}")

    # PHI patterns
    medical_record_pattern = re.compile(r'\b[A-Za-z]{2}\d{6}\b')
    # patient_name_pattern = re.compile(r'\b[A-Za-z]+\s[A-Za-z]+\b')
    dob_pattern = re.compile(r'\b\d{1,2}/\d{1,2}/\d{4}\b')

    # Combine all PHI patterns
    phi_pattern = re.compile(f"{medical_record_pattern.pattern}|{dob_pattern.pattern}") # |{patient_name_pattern.pattern}

    # PCI patterns
    visa_pattern = re.compile(r'\b4[0-9]{3}(?:[ -]?[0-9]{4}){3}\b')
    mastercard_pattern = re.compile(r'\b(?:5[1-5][0-9]{2}|2(?:2[2-9][0-9]|3[0-9]{2}|4[0-9]{2}|5[0-9]{2}|6[0-9]{2}|7[0-1][0-9]|720))(?:[ -]?[0-9]{4}){3}\b')
    amex_pattern = re.compile(r'\b3[47][0-9]{2}(?:[ -]?[0-9]{6})(?:[ -]?[0-9]{5})?\b')
    discover_pattern = re.compile(r'\b6(?:011|5[0-9]{2}|4[4-9][0-9]|2(?:2(?:1[2-9]|2[0-5])|[3-8][0-9]{2}|9(?:[01][0-9]|2[0-5])))(?:[ -]?[0-9]{4}){2}\b')
    # ach_number_pattern = re.compile(r'\b\d{9}\b')
    # swift_code_pattern = re.compile(r'\b[A-Za-z]{6}[A-Za-z0-9]{2}\b')


    # Combine all PCI patterns
    pci_pattern = re.compile(f"{visa_pattern.pattern}|{mastercard_pattern.pattern}|{amex_pattern.pattern}|{discover_pattern.pattern}") #|{ach_number_pattern.pattern}|{swift_code_pattern.pattern}")

    with open(file_path, 'r', encoding='utf-8', errors='ignore') as file:
        # Scan the file for sensitive information
        if file_path.lower().endswith('.docx'):
            content = read_docx(file_path)
        elif file_path.lower().endswith('.doc'):
            content = read_doc(file_path)
        elif file_path.lower().endswith('.xlsx'):
            content = read_excel(file_path)
        elif file_path.lower().endswith('.xls'):
            content = read_excel(file_path)
        elif file_path.lower().endswith('.pdf'):
            content = read_pdf(file_path)
        elif file_path.lower().endswith('.zip'):
            content = read_zip(file_path)
        else:
            content = read_text(file_path)

        pii_matches = re.findall(pii_pattern, content)
        phi_matches = re.findall(phi_pattern, content)
        pci_matches = re.findall(pci_pattern, content)

    return pii_matches, phi_matches, pci_matches