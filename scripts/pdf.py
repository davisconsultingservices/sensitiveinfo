from debug import print_debug_error
import fitz

def read_pdf(pdf_path):
    text = ""
    try:
        # Open the PDF file
        with fitz.open(pdf_path) as pdf_document:
            # Iterate through each page
            for page_number in range(pdf_document.page_count):
                # Get the page
                page = pdf_document[page_number]
                # Extract text from the page
                text += page.get_text()
    except Exception as e:
        # Handle exceptions, e.g., if the file is not a valid PDF
        print_debug_error(f"Error reading PDF: {e}")
    
    return text