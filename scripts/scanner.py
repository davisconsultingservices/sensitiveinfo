from debug import print_debug_error, print_debug
from patterns import scan_for_sensitive_info
from output import print_csv_results
import os
import sys

directory_to_scan = "/app/mnt"  # Default directory to scan

def scan_directory(directory_path):
    # Recursively scan files in the specified directory
    results = {'pii': [], 'phi': [], 'pci': []}

    try:
        for entry in os.scandir(directory_path):
            if entry.is_file():
                print_debug(" * file: "+entry.path)
                try:
                    pii_matches, phi_matches, pci_matches = scan_for_sensitive_info(str(entry.path))

                    if pii_matches:
                        pii_matches = [('PII', str(entry.path).removeprefix(directory_to_scan), pii_match) for pii_match in pii_matches]
                        results['pii'].extend(pii_matches)

                    if phi_matches:
                        phi_matches = [('PHI', str(entry.path).removeprefix(directory_to_scan), phi_match) for phi_match in phi_matches]
                        results['phi'].extend(phi_matches)

                    if pci_matches:
                        pci_matches = [('PCI', str(entry.path).removeprefix(directory_to_scan), pci_match) for pci_match in pci_matches]
                        results['pci'].extend(pci_matches)

                except PermissionError:
                    print_debug_error(f"Permission denied: {entry.path}")
                    continue

            elif entry.is_dir():
                # If the entry is a directory, recursively scan it
                rescursive_results = scan_directory(str(entry.path))
                results['pii'].extend(rescursive_results['pii'])
                results['phi'].extend(rescursive_results['phi'])    
                results['pci'].extend(rescursive_results['pci'])          

    except Exception as e:
        print_debug_error(f"Error while scanning directory: {e}")

    return results

if __name__ == "__main__":

    if not os.path.exists(directory_to_scan):
        print("Error: /app/mnt directory not found. Make sure to mount the correct directory.")
        sys.exit(1)

    # Perform the scanning
    scan_results = scan_directory(directory_to_scan)

    # Display the results in CSV format
    print_csv_results(scan_results)
