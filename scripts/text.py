from debug import print_debug_error

def read_text(file_path):
    try:
        with open(file_path, 'r', encoding='utf-8', errors='ignore') as file:
            content = file.read()
        return content
    except Exception as e:
        print_debug_error(f"Error reading {file_path}: {e}")
        return ""