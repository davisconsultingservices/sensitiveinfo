from debug import print_debug_error
import subprocess
from docx import Document

def read_doc(file_path):
    try:
        result = subprocess.run(["antiword", file_path], capture_output=True, text=True)
        content = result.stdout
        return content
    except Exception as e:
        print_debug_error(f"Error reading {file_path} with antiword: {e}")
        return ""

def read_docx(file_path):
    try:
        doc = Document(file_path)
        content = ""
        for paragraph in doc.paragraphs:
            content += paragraph.text + "\n"
        return content
    except Exception as e:
        print_debug_error(f"Error reading {file_path}: {e}")
        return ""