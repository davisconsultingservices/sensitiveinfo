import zipfile

def read_zip(file_path):
    with zipfile.ZipFile(file_path, 'r') as zip_file:
        file_contents = ""
        for file_info in zip_file.infolist():
            with zip_file.open(file_info) as file:
                file_contents += file.read().decode('utf-8', errors='ignore')
    return file_contents
